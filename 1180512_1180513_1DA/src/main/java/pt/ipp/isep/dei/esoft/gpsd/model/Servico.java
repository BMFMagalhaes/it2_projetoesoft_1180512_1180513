package pt.ipp.isep.dei.esoft.gpsd.model;

public interface Servico {

	void getOutrosAtributos();

	void setDadosAdicionais();

	void getTipoServiso();

	void getCategoria();

	void possuiOutrosAtributos();

}