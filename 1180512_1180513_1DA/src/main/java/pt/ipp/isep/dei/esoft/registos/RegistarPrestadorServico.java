package pt.ipp.isep.dei.esoft.registos;

public class RegistarPrestadorServico {
    private String nomeC;
    private String nomeA;
    private String id;
    private String email;
    
    /**
     *
     * @param nomeC
     * @param nomeA
     * @param id
     * @param email
     */
    public void novoPrestadorDeServicos(String nomeC, String nomeA, String id, String email) {
        if (nomeC == null || nomeA == null || id == null || email == null || nomeC.isEmpty() || nomeA.isEmpty() || id.isEmpty() || email.isEmpty()){
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.nomeC = nomeC;
        this.nomeA = nomeA;
        this.email = email;
        this.id = id;
    }

}
