package pt.ipp.isep.dei.esoft;

public class PreferenciaHorario {

    /*
    A ordem pela qual a preferência foi indicada.
    */
    private String ordem;
    
    /*
    A data preferida para a realização do serviço.
    */
    private String data;
    
    /*
    A hora preferida para a realização do serviço.
    */
    private String hora;

    /**
     *Construtor do horário preferido pelo cliente.
     * @param ordem a posicao da preferencia indicada pelo cliente
     * @param data A data preferida para a realização do serviço
     * @param hora A hora preferida para a realização do serviço
     */
    public PreferenciaHorario(String ordem, String data, String hora) {
        if (ordem == null || data == null || hora == null || ordem.isEmpty() || data.isEmpty() || hora.isEmpty()) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.ordem = ordem;
        this.data = data;
        this.hora = hora;
    }

    /**
     * @return a posição da ordem
     */
    public String getOrdem() {
        return ordem;
    }

    /**
     * @return a data preferida pelo cliente
     */
    public String getData() {
        return data;
    }

    /**
     * @return a hora preferida pelo cliente
     */
    public String getHora() {
        return hora;
    }

    
}
