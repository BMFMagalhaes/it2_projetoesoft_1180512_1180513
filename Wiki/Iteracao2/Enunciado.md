# Enunciado Iteração 1

Pretende-se desenvolver uma aplicação informática para apoio à atividade de empresas dedicadas à prestação de serviços ao domicilio variados, tais como a execução de tarefas quotidianas (e.g. limpeza) como tarefas mais ocasionais (e.g.  desentupimentos, manutenção de estores e fechaduras).

Desde já, perspetiva-se que a aplicação será acedida por vários utilizadores com diferentes papeis, tais como:

* **Cliente**: pessoa que se registou na aplicação com o intuito de efetuar pedidos de prestação de serviço(s);
* **Prestador Serviços**: pessoa responsável por executar os serviços solicitados pelos clientes à empresa;
* **Administrativo e Funcionário de Recursos Humanos**: pessoas responsáveis por realizar no sistema várias atividades de suporte à atividade da empresa.  

As interações dos utilizadores supramencionados devem ser precedidas de um processo de autenticação. A utilização da aplicação por outras pessoas está restrita a: (i) efetuar registo como cliente e (ii) submeter candidatura a prestador de serviço. Em ambos os casos é obrigatório requer o nome completo da pessoa, o seu número de identificação fiscal (NIF), contacto telefónico, endereço de correio eletrónico (email) e  pelo menos um endereço postal. No caso especifico da candidatura, o sistema deve solicitar ainda habilitações académicas e profissionais, a anexação de documentos comprovativos da informação prestada e a indicação das categorias de serviços que se propõe realizar. Após o registo como cliente, o mesmo pode aceder imediatamente ao sistema.

Cabe aos administrativos da empresa, especificar os vários serviços prestados, bem como as categorias em que os serviços estão catalogados. Uma categoria basicamente corresponde a um código único e uma descrição. Por outro lado, cada serviço possui um identificar único, uma descrição breve e outra completa, é catalogado numa categoria e tem um custo/hora.

Aquando do pedido de prestação de serviços, o cliente deve indicar, por ordem de preferência, os horários (data/hora inicio) pretendidos (mínimo um) e complementar cada serviço solicitado com uma descrição da tarefa pretendida. Alguns serviços podem requerer a indicação de uma quantidade de tempo (e.g. limpeza geral por 3 horas). Para outros serviços, usa-se como referência uma estimativa de tempo médio. O sistema deve informar o cliente do custo estimado do seu pedido e atribuir automaticamente um número de pedido sequencial. Salienta-se ainda que a cada pedido podem acrescer outros custos adicionais. De momento, considere-se apenas o custo de deslocação que varia em função da área geográfica do domicilio em que prestação de serviço ocorrerá. Dentro da mesma área geográfica o valor é fixo (e.g. Porto: 10€; Lisboa: 12€). As áreas geográficas e respetivos custos de deslocação são especificados pelos administrativos.

No desenvolvimento deste sistema a equipa de desenvolvimento deve: (i) adotar boas práticas de identificação de requisitos e de análise e design de software OO; (ii) implementar o núcleo principal do software em Java; (iii) adotar normas de codificação reconhecidas e (iv) reutilizar o componente de gestão de utilizadores existente na empresa baseado em identificador de utilizador (i.e. email) e palavra-passe (cf. [documentação](GestaoUtilizadores)).

A informação da empresa (e.g. designação e NIF) que está a usar o software deve ser especificada por configuração.

# Enunciado Iteração 2

1.1 Resposta	a	questões	em	aberto:

1. Não	 podem	 existir	 clientes	 (i)	 com	 o	 mesmo	 NIF	 nem	 (ii)	 com	 mesmo endereço	de correio eletrónico.
2. A área	geográfica correspondente a um	endereço é determinada através do código postal constante	no respetivo endereço(cf.	novos	requisitos).
3. Aquando de	um pedido de	prestação	de serviço:
  3.1. O cliente pode querer indicar um endereço postal que ainda não está associado à sua informação	pessoal. Neste caso, o sistema deve permitir que o cliente associe à sua informação	pessoal	um novo	endereço postal.
  3.2. O sistema deve enviar ao cliente um email de notificação com a informação do	pedido realizado e o seu estado	atual	(i.e.	submetido).
  3.3. O tempo mínimo para execução	de um	qualquer serviço é de	30 minutos, sendo	apenas possível	solicitar	múltiplos	deste	valor.
  3.4. Um	ou mais	serviços podem ser solicitados;
  3.5. A preferência de	horários indicada aplica-se a todos os serviços solicitados no mesmo pedido.
4. Aquando da especificação de um serviço, deve indicar-se qual é o tipo de serviço. De	momento, devem ser suportados os seguintes tipos:
  4.1. Serviços	Fixos: são aqueles para	os quais a empresa pré-determina (e.g. com base numa estimativa) uma duração, independentemente	da sua execução ser mais ou menos demorada. É o valor desta duração que é sempre	cobrado	ao cliente.
  4.2. Serviços Limitados: são aqueles para os quais não existe uma duração pré-determinada e por conseguinte, aquando do seu pedido, o	cliente deve estipular o tempo pretendido. Estes serviços	são	executados até ao	limite de	tempo	indicado pelo	cliente, findo o qual	a	prestação	do serviço é	dada	como	terminada.
  4.3. Serviços Expansíveis:	são	semelhantes	aos	serviços	limitados, contudo o tempo indicado	pelo cliente pode	ser	excedido para	permitir a	conclusão	adequada do	serviço.


1.2 Novos	Requisitos

Cada área geográfica centra-se num determinado código	postal base	(e.g.	“4249-015”)	e	atua em	todos	os endereços cujo	código postal esteja no	seu	raio de	ação (e.g. 5 km). Para obter os códigos postais no raio de um outro código postal, deve-se recorrer a um serviço externo definido por configuração aquando do momento de instalação do sistema. Para este efeito, o sistema deve suportar diferentes serviços externos. Na eventualidade de um endereço postal ser coberto por	mais do que	uma	área geográfica, adota-se aquela cuja distância é menor.
Os prestadores de	serviços contratados pela	empresa são	registados no	sistema pelos Funcionários de	Recursos Humanos (FRH).	Esta tarefa, entre outros	dados, requer	o número mecanográfico do	prestador, o seu nome	completo e abreviado, o	email institucional, as áreas	geográficas	onde presta	serviços e a indicação das categorias dos serviços que está apto a executar. Findo o registo de um prestador, o sistema deve gerar a informação necessária para este aceder ao sistema e enviar	a	mesma	por	email.
De modo a poder-se atribuir os pedidos de prestação de serviços realizados pelos clientes pelos diferentes prestadores, é necessário que estes indiquem previamente no sistema as suas disponibilidades diárias. A este respeito, salienta-se que para	o mesmo	dia	um prestador de	serviço	pode indicar mais	do que um	período	de disponibilidade. Deve-se também	considerar aspetos relacionados	com	a internacionalização	da aplicação,	nomeadamente em termos de suporte a múltiplos idiomas e fusos horários. Exige-se ainda que o sistema tenha uma taxa mínima de testes de cobertura	e	mutação	(e.g.	unitários, funcionais	e	de integração) de	95%.
