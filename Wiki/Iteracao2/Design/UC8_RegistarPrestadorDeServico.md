# Realização de UC8 Registar Prestador de Serviço

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. O funcionário dos recursos humanos (FRH) inicia o registo do prestador de serviços. | ...interage com o utilizador? | RegistarPrestadorDeServicoUI | PureFabrication |
| | ...coordena o UC? | RegistarPrestadorDeServicoController | Controller |
| | ... cria/instância PrestadorDeServicos? | Empresa | Creator (Regra 1) |
| 2. O sistema solicita os dados necessários (i.e. nome completo, nome abreviado, número mecanográfico, email). ||||
| 3. O funcionário dos recursos humanos introduz os dados solicitados. | ...guarda os dados introduzidos? | PrestadorDeServicos | Information Expert (IE) - instância criada no passo 1 |
| 4. O sistema mostra as áreas geográficas e solicita uma onde presta serviço. | ...conhece as áreas geográficas? | Empresa | IE : No MD a Empresa possui as Áreas Geográficas |
| 5. O funcionário dos recursos humanos seleciona a área geográfica. | ...guarda a área geográfica selecionada? | PrestadorDeServicos | Information Expert (IE) - instância criada no passo 1 |
| 6. O sistema repete os passos 4 e 5 até todas as áreas geográficas pretendidas estarem determinadas. | | | |
| 7. O sistema mostra as categorias e solicita a que está apto a atuar. | ...conhece as categorias? | Empresa | IE_ No MD a Empresa possui Categorias |
| 8. O funcionário dos recursos humanos seleciona a categoria. | ...guarda a categoria selecionada? | PrestadorDeServicos| Information Expert (IE) - instância criada no passo 1 |
| 9. O sistema repete os passos 7 e 8 até todas as categorias pretendidas estarem selecionadas. | | | |
| 10. O sistema apresenta e pede confirmação dos dados. | ...valida os dados do prestador de serviços (validação local)? | PrestadorDeServicos | IE: A classe possui os seus dados. |
| | ...valida os dados do prestador de serviços (validação global)? | Empresa ou FRH?| IE: A Empresa contém os prestadores de serviços. |
| 11. O funcionário dos recursos humanos confirma. ||||
| 12. O sistema envia um email ao prestador de serviços com as informações necessárias para aceder ao sistema e informa o funcionário dos recursos humanos do sucesso da operação. | ...notifica o utilizador? | RegistarPrestadorDeServicoUI ||
|| ...envia o email ao prestador de serviços?| Empresa |||

## Sistematização ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* PrestadorDeServicos

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistarPrestadorDeServicoUI  
 * RegistarPrestadorDeServicoController

## Diagrama de Sequência

![SD_UC8_IT2.png](SD_UC8_IT2.png)

## Diagrama de Classes

![CD_UC8_IT2.png](CD_UC8_IT2.png)
