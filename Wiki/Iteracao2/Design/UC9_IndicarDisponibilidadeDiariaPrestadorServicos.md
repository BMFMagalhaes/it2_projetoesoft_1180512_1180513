# Realização de UC5 Especificar Área Geográfica

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. O prestador de servicos inicia a indicaçao da disponibilidade diaria.| ... interage com o utilizador?| IndicarDisponibilidadeUI| PureFabrication, pois não se justifica atribuir esta responsabilidade a outra classe qualquer do modelo de domínio. |
||...coordena o UC?|IndicarDisponibilidadeController| Controller |
||...cria/instancia Disponibilidade?|Empresa| Creator (Regra 1) |
| 2. O sistema solicita os dados necessários (i.e. dia da semana, hora de início e hora do fim).| | | |
| 3. O prestador de serviços introduz os dados solicitados. | ... guarda os dados introduzidos?| RegistarDisponibilidade | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.| ...valida os dados da Disponibilidade (validação local)?| Disponibilidade | IE: Disponibilidade possui os seus próprios dados|
| | ...valida os dados da Disponibilidade (validação global)?| RegistarDisponibilidade | IE: RegistarDisponibilidade contém/agrega Disponibilidade|| 5. O prestador de serviços confirma.| | | |
| 6. O sistema regista os dados e informa o prestador de servicos do sucesso da operação.| ...guarda a Disponibilidade especificada/criada?| Empresa | IE. No modelo de domino a Empresa contém/agrega Disponibilidade |
|| ... notifica o utilizador?                                                                                   | DisponibilidadeUI                                        |                                                |                                                                                                                      

## Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Empresa
 * <<interface>> disp: Disponibilidade

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * IndicarDisponibilidadeUI  
 * IndicarDisponibilidadeController
 * RegistarDisponibilidade


##	Diagrama de Sequência

![SD_UC9_IT2.png](SD_UC9_IT2.png)


##	Diagrama de Classes

![CD_UC9_IT2.png](CD_UC9_IT2.png)
