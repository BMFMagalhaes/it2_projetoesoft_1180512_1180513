# Realização de UC4 Especificar Serviço

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. O administrativo inicia a especificação de um novo serviço.| ... interage com o utilizador?| EspecificarServicoUI| PureFabrication |
||...coordena o UC?|EspecificarServicoController| Controller |
||...cria/instancia Servico?|Empresa| Creator (Regra 1) |
| 2. O sistema mostra os tipos de serviço (i.e. fixo, limitado ou expansível ) e pede para selecionar um.|...conhece os tipos de serviço a listar? | Empresa | IE: Empresa tem os Servico|
| 3. O administrativo introduz o tipo de serviço.| ...guarda o tipo de serviço introduzido?| Servico | IE: Tipos de serviço definidos na classe Servico |
| 4. **O sistema mostra a lista de categorias existentes para que seja selecionada uma.**| ...conhece as categorias existentes a listar?| Empresa| IE: Empresa tem/agrega todas as Categoria|
| 5. **O administrativo seleciona a categoria em que pretende catalogar o serviço.**| ... guarda a categoria selecionada?| Servico| IE: Servico catalogado numa Categoria - instância criada no passo 1|
| 6. O sistema solicita os dados necessários (i.e. identificador único, descrição breve e completa e o custo/hora).| | | |
| 7. O administrativo introduz os dados solicitados. | ... guarda os dados introduzidos?| Servico | Information Expert (IE) - instância criada no passo 1|
| 8. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.| ...valida os dados do Serviço (validação local)?| Servico| IE: Servico possui os seus próprios dados|
| | ...valida os dados do Serviço (validação global)?| Empresa | IE: A Empresa contém/agrega Serviços|
| 9. O administrativo confirma.| | | |
| 10. O sistema regista os dados e informa o administrativo do sucesso da operação.| ...guarda o Servico especificado/criado?| Empresa | IE. No MD a Empresa contém/agrega Servicos |
|| ... notifica o utilizador?                                                                                   | EspecificarServicoUI                                        |       ||                                                                                                                      

## Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Empresa
 * Servico
 * Categoria

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarServicoUI  
 * EspecificarServicoController


##	Diagrama de Sequência

![SD_UC4_IT2.png](SD_UC4_IT2.png)


##	Diagrama de Classes

![CD_UC4_IT2.png](CD_UC4_IT2.png)
