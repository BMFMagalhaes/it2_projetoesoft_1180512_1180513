# Realização de UC5 Especificar Área Geográfica

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
  | 1. O cliente inicia a associação de um endereço postal.| ... interage com o utilizador?| AssociarEnderecoPostalAClienteUI| PureFabrication, pois não se justifica atribuir esta responsabilidade a outra classe qualquer do modelo de domínio. |
||...coordena o UC?|AssociarEnderecoPostalAClienteController| Controller |
||...cria/instancia EnderecoPostal?|Empresa| Creator (Regra 1) |
| 2. O sistema solicita os dados necessários (i.e. endereço, codigo postal, localidade).| | | |
| 3. O cliente introduz os dados solicitados. | ... guarda os dados introduzidos?| RegistarEndereco | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.| ...valida os dados do EnderecoPostal (validação local)?| EndereçoPostal| IE: EnderecoPostal possui os seus próprios dados|
| | ...valida os dados da EnderecoPostal (validação global)?| RegistarEndereco | IE: A RegistarEndereco contém/agrega EnderecoPostal|
| 5. O cliente confirma.| | | |
| 6. O sistema regista os dados e informa o cliente do sucesso da operação.| ...guarda o EnderecoPostal especificada/criada?| RegistarEndereco | IE. No modelo de domínio o Cliente contém/agrega EnderecoPostal |
|| ... notifica o utilizador?| AssociarEnderecoPostalAClienteUI||                                                        

## Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Empresa
 * <<interface>> ep: EnderecoPostal

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AssociarEnderecoPostalAClienteUI  
 * AssociarEnderecoPostalAClienteController
 * RegistarCliente
 * RegistarEndereco



##	Diagrama de Sequência

![SD_UC7_IT2.png](SD_UC7_IT2.png)


##	Diagrama de Classes

![CD_UC7_IT2.png](CD_UC7_IT2.png)
