# Realização de UC5 Especificar Área Geográfica

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. O administrativo inicia a especificação de uma nova áreas geográfica.| ... interage com o utilizador?| EspecificarAreaGeograficaUI | PureFabrication, pois não se justifica atribuir esta responsabilidade a outra classe qualquer do modelo de domínio. |
||...coordena o UC?|EspecificarAreaGeograficaController| Controller |
||...cria/instancia AreaGeografica?|Empresa| Creator (Regra 1) |
| 2. O sistema solicita os dados necessários (i.e. designação, custo do transporte, código postal e raio de atuação).| | | |
| 3. O administrativo introduz os dados solicitados. | ... guarda os dados introduzidos?| AreaGeografica | Information Expert (IE) - instância criada no passo 1|
||...determina que códigos postais estao dentro do raio de atuação?|ServicoExterno|IE: ServicoExterno possui os seus próprios dados|
||...calcula a distância entre os códigos postais?|ServicoExterno|IE: ServicoExterno possui os seus próprios dados|
| 4. O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, e apresenta os códigos postais abrangidos por essa área pedindo que os confirme| ...valida os dados da Área Geográfica (validação local)?| AreaGeografica| IE: AreaGeografica possui os seus próprios dados|
| | ...valida os dados da AreaGeografica (validação global)?| RegistoAreaGeografica | IE: O RegistoAreaGeografica contém/agrega AreaGeografica|
| 5. O administrativo confirma.| | | |
| 6. O sistema regista os dados e informa o administrativo do sucesso da operação.| ...guarda a AreaGeografica especificada/criada?| Empresa | IE. No modelo de dominio a Empresa contém/agrega AreaGeografica |
|...|notifica o utilizador?|EspecificarAreaGeograficaUI||                                                                                                                                                            

## Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Empresa
 * AreaGeografica

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarAreaGeograficaUI  
 * EspecificarAreaGeograficaController
 * RegistoAreaGeografica
 * ServicoExterno


##	Diagrama de Sequência

![SD_UC5_IT2.png](SD_UC5_IT2.png)


##	Diagrama de Classes

![CD_UC5_IT2.png](CD_UC5_IT2.png)
