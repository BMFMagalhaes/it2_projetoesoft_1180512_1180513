# UC9 - Indicar Disponibilidade Diária do Prestador de Serviços

## Formato Breve

O prestador de serviços inicia a indicação da sua disponibilidade diária. O sistema solicita os dados necessários (i.e. dia da semana, hora de início e hora do fim). O prestador de serviços introduz os dados solicitados. O sistema valida e apresenta os dados ao prestador de serviços, pedindo que os confirme. O prestador de serviços confirma. O sistema regista os dados e informa o prestador do sucesso da operação.

## SSD
![SSD_UC5_IT9_IndicarDisponibilidadeDiariaPrestadorServicos.png](SSD_UC5_IT9_IndicarDisponibilidadeDiariaPrestadorServicos.png)

## Formato Completo

### Ator principal

Administrativo

### Partes interessadas e seus interesses
* **Administrativo:** pretende saber em que horários possui prestadores de serviços.
* **Prestador de Serviços:** pretende especificar em que horário está disponível para prestar serviços.
* **Cliente:** pretende saber em que horas pode pedir um serviço.
* **Empresa:** pretende que os horários dos prestadores de serviços sejam descritos com rigor/detalhe.


### Pré-condições
Existência de prestador de serviços

### Pós-condições
A disponibilidade diária do prestador de serviços é registada no sistema.

## Cenário de sucesso principal (ou fluxo básico)

1. O prestador de serviços inicia a especificação da sua disponibilidade diária.
2. O sistema solicita os dados necessários (i.e. hora de início e hora do fim possíveis).
3. O prestador de serviços introduz os dados solicitados.
5. O sistema valida e apresenta os dados ao prestador de serviços, pedindo que os confirme.
6. O prestador de serviços confirma.
8. O sistema regista os dados e informa o prestador de serviços do sucesso da operação.

### Extensões (ou fluxos alternativos)

*a. O prestador de serviços solicita o cancelamento da indicação da disponibilidade diária do prestador de serviços.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta.
> 2. O sistema permite a introdução dos dados em falta (passo 3)
>
	> 2a. O prestador de serviços não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o prestador de serviços para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O prestador de serviços não altera os dados. O caso de uso termina.

### Requisitos especiais
\-

### Lista de Variações de Tecnologias e Dados
\-

### Frequência de Ocorrência
\-

### Questões em aberto

* Caso o prestador de serviços não tenha disponibilidade para um certo dia, como é isso declarado?
* Qual a frequência de ocorrência deste caso de uso?
