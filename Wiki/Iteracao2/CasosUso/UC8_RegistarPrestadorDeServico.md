# UC8 - Registar Prestador De Serviço

## Formato Breve

O funcionário dos recursos humanos (FRH) inicia o registo do prestador de serviços. O sistema solicita os dados necessários (i.e. nome completo, nome abreviado, número mecanográfico, email). O funcionário dos recursos humanos introduz os dados solicitados. O sistema mostra as áreas geográficas e solicita uma onde presta serviço. O funcionário dos recursos humanos seleciona a área geográfica. O sistema repete os dois últimos passos até todas as áreas geográficas pretendidas estarem determinadas. O sistema mostra as categorias e solicita a que está apto a atuar. O funcionário dos recursos humanos seleciona a categoria. O sistema repete os dois últimos passos até todas as categorias pretendidas estarem selecionadas. O sistema apresenta e pede confirmação dos dados. O funcionário dos recursos humanos confirma. O sistema envia um email ao prestador de serviços com as informações necessárias para aceder ao sistema e informa o funcionário dos recursos humanos do sucesso da operação.

## SSD
![UC8-SSD-IT2.png](SSD_UC8_IT2.png)


## Formato completo

## Ator Principal

Funcionário dos recursos humanos (FRH)

### Partes interessadas e seus interesses
* **Funcionário dos recursos humanos:** Pretende que o prestador de serviços fique registado com sucesso no sistema e que este tenha acesso ao mesmo.
* **Prestador De Serviços:** Pretende ter acesso ao sistema para poder realizar os serviços.
* **Empresa:** Pretende que os prestadores de serviço tenham acesso ao sistema para que possam prestar serviços aos seus clientes.


### Pré-condições
As categorias e as áreas geográficas têm de estar definidas no sistema.

### Pós-condições
Os prestadores de serviços ficam registados no sistema e com acesso ao sistema.

## Cenário de sucesso principal (ou fluxo básico)

1. O funcionário dos recursos humanos (FRH) inicia o registo do prestador de serviços.
2. O sistema solicita os dados necessários (i.e. nome completo, nome abreviado, número mecanográfico, email)
3. O funcionário dos recursos humanos introduz os dados solicitados.
4. O sistema mostra as áreas geográficas e solicita uma onde presta serviço.
5. O funcionário dos recursos humanos seleciona a área geográfica.
6. O sistema repete os passos 4 e 5 até todas as áreas geográficas pretendidas estarem determinadas.
7. O sistema mostra as categorias e solicita a que está apto a atuar.
8. O funcionário dos recursos humanos seleciona a categoria.
9. O sistema repete os passos 7 e 8 até todas as categorias pretendidas estarem selecionadas.
10. O sistema apresenta e pede confirmação dos dados.
11. O funcionário dos recursos humanos confirma.
12. O sistema envia um email ao prestador de serviços com as informações necessárias para aceder ao sistema e informa o funcionário dos recursos humanos do sucesso da operação.


### Extensões (ou fluxos alternativos)

*a. O funcionário dos recursos humanos solicita o cancelamento do registo do prestador de serviços.

> O caso de uso termina.

4a. Dados obrigatórios em falta.
> 1. O sistema informa quais os dados em falta.
> 2. O sistema permite a introdução dos dados em falta. (passo 3)
>
 > 2a. O funcionário dos recursos humanos não altera os dados. O caso de uso termina.

 ### Requisitos especiais
 \-

 ### Lista de Variações de Tecnologias e Dados
 \-

 ### Frequência de Ocorrência
 \-

 ### Questões em aberto
