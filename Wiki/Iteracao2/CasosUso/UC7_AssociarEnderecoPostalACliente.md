# UC7 - Associar Endereço Postal a Cliente

## Formato Breve

O administrativo inicia a associação de um endereço postal a um cliente. O sistema solicita os dados necessários (i.e. endereço, código postal, localidade). O cliente introduz os dados solicitados. O sistema valida e apresenta os dados ao cliente. O cliente confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

## SSD
![SSD_UC7_IT2_AssociarEnderecoPostalACliente.png](SSD_UC7_IT2_AssociarEnderecoPostalACliente.png)

## Formato Completo

### Ator principal

Cliente

### Partes interessadas e seus interesses
* **Administrativo:** pretende saber o endereço postal do cliente.
* **Cliente:** pretende associar um endereço postal a si mesmo para poder efetuar uma prestação de serviços.
* **Empresa:** pretende que o endereço postal seja descrito em rigor/detalhe.



### Pré-condições
Existência de pelo menos um cliente

### Pós-condições
O endereço postal é associado a um cliente e é registado no sistema.

## Cenário de sucesso principal (ou fluxo básico)

1. O cliente inicia a associação do seu endereço postal.
2. O sistema solicita os dados necessários  (i.e. rua da habitação, número da porta, código postal, cidade, país).
3. O cliente introduz os dados solicitados.
5. O sistema valida e apresenta os dados ao cliente, pedindo que os confirme.
6. O cliente confirma.
8. O sistema regista os dados e informa o cliente do sucesso da operação.

### Extensões (ou fluxos alternativos)

*a. O cliente solicita o cancelamento da associação de um endereço postal.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta.
> 2. O sistema permite a introdução dos dados em falta (passo 3)
>
	> 2a. O cliente não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o cliente para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O cliente não altera os dados. O caso de uso termina.

### Requisitos especiais
\-

### Lista de Variações de Tecnologias e Dados
\-

### Frequência de Ocorrência
\-

### Questões em aberto

* Existem outros dados que são necessários para identificar o endereço postal?
* Quais  os dados que são obrigatórios para identificar um endereço postal?
* Qual a frequência de ocorrência deste caso de uso?
