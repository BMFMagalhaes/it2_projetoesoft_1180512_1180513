# Realização dos Casos de Uso (DS + DC)


| UC  | Descrição                                                               |                   
|:----|:------------------------------------------------------------------------|
| UC1 | [Efetuar Registo como Cliente](Design/UC1_EfetuarRegistoCliente.md)   |
| UC2 | [Submeter Candidatura a Prestador Serviço](Design/UC2_SubmeterCandidaturaPrestadorServico.md)  |
| UC3 | [Especificar Categoria (de Serviço)](Design/UC3_EspecificarCategoria.md)|
| UC4 | [Especificar Serviço](Design/UC4_EspecificarServico.md)|
| UC5 | [Especificar Área Geográfica](Design/UC5_EspecificarAreaGeografica.md) |
| UC6 | [Efetuar Pedido Prestação de Serviços](Design/UC6_EfetuarPedidoPrestacaoServicos.md)|
