# Enunciado Iteração 1

Pretende-se desenvolver uma aplicação informática para apoio à atividade de empresas dedicadas à prestação de serviços ao domicilio variados, tais como a execução de tarefas quotidianas (e.g. limpeza) como tarefas mais ocasionais (e.g.  desentupimentos, manutenção de estores e fechaduras).

Desde já, perspetiva-se que a aplicação será acedida por vários utilizadores com diferentes papeis, tais como: 

* **Cliente**: pessoa que se registou na aplicação com o intuito de efetuar pedidos de prestação de serviço(s);
* **Prestador Serviços**: pessoa responsável por executar os serviços solicitados pelos clientes à empresa;
* **Administrativo e Funcionário de Recursos Humanos**: pessoas responsáveis por realizar no sistema várias atividades de suporte à atividade da empresa.  

As interações dos utilizadores supramencionados devem ser precedidas de um processo de autenticação. A utilização da aplicação por outras pessoas está restrita a: (i) efetuar registo como cliente e (ii) submeter candidatura a prestador de serviço. Em ambos os casos é obrigatório requer o nome completo da pessoa, o seu número de identificação fiscal (NIF), contacto telefónico, endereço de correio eletrónico (email) e  pelo menos um endereço postal. No caso especifico da candidatura, o sistema deve solicitar ainda habilitações académicas e profissionais, a anexação de documentos comprovativos da informação prestada e a indicação das categorias de serviços que se propõe realizar. Após o registo como cliente, o mesmo pode aceder imediatamente ao sistema.

Cabe aos administrativos da empresa, especificar os vários serviços prestados, bem como as categorias em que os serviços estão catalogados. Uma categoria basicamente corresponde a um código único e uma descrição. Por outro lado, cada serviço possui um identificar único, uma descrição breve e outra completa, é catalogado numa categoria e tem um custo/hora.

Aquando do pedido de prestação de serviços, o cliente deve indicar, por ordem de preferência, os horários (data/hora inicio) pretendidos (mínimo um) e complementar cada serviço solicitado com uma descrição da tarefa pretendida. Alguns serviços podem requerer a indicação de uma quantidade de tempo (e.g. limpeza geral por 3 horas). Para outros serviços, usa-se como referência uma estimativa de tempo médio. O sistema deve informar o cliente do custo estimado do seu pedido e atribuir automaticamente um número de pedido sequencial. Salienta-se ainda que a cada pedido podem acrescer outros custos adicionais. De momento, considere-se apenas o custo de deslocação que varia em função da área geográfica do domicilio em que prestação de serviço ocorrerá. Dentro da mesma área geográfica o valor é fixo (e.g. Porto: 10€; Lisboa: 12€). As áreas geográficas e respetivos custos de deslocação são especificados pelos administrativos.

No desenvolvimento deste sistema a equipa de desenvolvimento deve: (i) adotar boas práticas de identificação de requisitos e de análise e design de software OO; (ii) implementar o núcleo principal do software em Java; (iii) adotar normas de codificação reconhecidas e (iv) reutilizar o componente de gestão de utilizadores existente na empresa baseado em identificador de utilizador (i.e. email) e palavra-passe (cf. [documentação](GestaoUtilizadores)).

A informação da empresa (e.g. designação e NIF) que está a usar o software deve ser especificada por configuração.









